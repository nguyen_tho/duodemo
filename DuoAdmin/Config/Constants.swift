//
//  Constants.swift
//  eschool
//
//  Created by nguyen.manh.tuanb on 8/15/18.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import Foundation

struct Constants {
    
    // MARK: Server
    struct Server {
       static let domainName = "https://api-3c05b394.duosecurity.com"
        static let auth = "Authorization"
        static let date = "Date"
        static let host = "Host"
        static let agent = "User-Agent"
        static let contentType = "Content-Type"
        static let version = "v2"
    }
    
    // MARK: UserDefaultsKey
    struct UserDefaultsKey {
        static let AccessToken = "AccessToken"
    }

    // MARK: APIKey
    struct DuoKey {
        static var ikey = "DIUQETA03CHWTSRKE22J"//"DIU2L2IZKMNDVBJ7N4UX"
        static var skey = "61jsXThS9qOvvR4lHPNje59fe9bcwRArnhbkH1CU"//"TVPspiepaluZBvXDno45eLFokDF5PCCKq3f72lxs"
        static var host = "api-3c05b394.duosecurity.com"//"api-5c9602c9.duosecurity.com"
    }
    
    struct DuoKeyWord {
        static var ikey = "ikey"
        static var skey = "skey"
        static var host = "host"
    }
}
