//
//  Enum.swift
//  ThachSanh
//
//  Created by nguyen.manh.tuanb on 11/10/2018.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import Foundation

enum Menu: Int {
    case Home
    case OderList
    case Buy
    case Sale
    case Delivery
    case Contact
    case Setting
    case Profile
}

enum PushType: String {
    case non
    case qa
}

enum OrderType: Int {
    case unknown
    case watitingPayment
    case getOrder
    case confirmTransport
    case transportCompleted
}
