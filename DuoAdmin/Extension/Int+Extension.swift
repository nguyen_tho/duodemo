//
//  Int+Extension.swift
//  eschool
//
//  Created by nguyen.manh.tuanb on 8/16/18.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import Foundation

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self)) ?? ""
    }
}
