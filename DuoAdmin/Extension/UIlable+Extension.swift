//
//  UIlable+Extension.swift
//  GogoCar
//
//  Created by nguyen.manh.tuanb on 9/14/18.
//  Copyright © 2018 Nguyen Manh Tuan. All rights reserved.
//

import UIKit

extension UILabel {
    
    func isTouchOn(point: CGPoint, inTextRange range: NSRange) -> Bool {
        guard let textAttribute = self.attributedText else { return false }
        let textStorage = NSTextStorage(attributedString: textAttribute)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)
        
        let index = layoutManager.characterIndex(for: point,
                                                 in: textContainer,
                                                 fractionOfDistanceBetweenInsertionPoints: nil)
        
        if index >= range.location && index < range.location + range.length {
            return true
        }
        
        return false
    }
    
    func underLine(text: String) {
        let attributes = text.underLineAtrribute()
        self.attributedText = attributes
    }
}
