//
//  Double+Extenstion.swift
//  GogoCar
//
//  Created by Nguyen Manh Tuan on 9/11/18.
//  Copyright © 2018 Nguyen Manh Tuan. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func toKm() -> String {
        let distance = self.description
        let result = distance.replacingOccurrences(of: ".", with: ",") + " km"
        return result
    }
}
