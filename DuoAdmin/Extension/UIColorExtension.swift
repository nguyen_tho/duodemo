//
//  UIColorExtension.swift
//  eschool
//
//  Created by nguyen.huu.hoang on 8/22/18.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

extension UIColor {
    class func colorWithHexaCode(_ hexCode: String) -> UIColor {
        return UIColor.colorWithHexaCode(hexCode, alpha: 1.0)
    }
    
    class func colorWithHexaCode(_ hexCode: String, alpha: CGFloat) -> UIColor {
        var hex = hexCode
        if hex.hasPrefix("#") {
            let index = hex.index(hex.startIndex, offsetBy: 1)
            hex = String(hex[index...])
        }
        guard hex.count == 6 else {
            return UIColor.gray
        }
        var rgbValue: UInt32 = 0
        Scanner(string: hex).scanHexInt32(&rgbValue)
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(rgbValue & 0x0000FF) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
