//
//  UINavigationController+Extension.swift
//  ThachSanh
//
//  Created by nguyen.manh.tuanb on 11/10/2018.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

extension UINavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
