//
//  UserToolsViewController.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class UserToolsViewController: BaseViewController {
    
    private var users: [UserMoodel] = []
    
    @IBOutlet weak var tbView: UITableView!
    
    override func setupView() {
        tbView.tableFooterView = UIView()
        tbView.register(UserTableViewCell.self)
        tbView.delegate = self
        tbView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUser()
    }
    
    @IBAction func createUser(_ sender: Any) {
        let alertController = UIAlertController(title: "", message: "Create user", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "User name"
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        
        alertController.addAction(
            UIAlertAction(title: "OK", style: .default, handler: { [unowned self] _ in
                guard let nameTf = alertController.textFields?.first else { return }
                guard let emailTf = alertController.textFields?.last else { return }
                guard let name = nameTf.text, !name.isEmpty else { return }
                guard let email = emailTf.text, !email.isEmpty, email.isValidEmail() else { return }
                self.createUser(name: name, email: email)
            }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    private func loadUser() {
        startAnimating()
        APIService.instance.sendRequest(API.users) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard let result = result else {
                self.showAlert(error.localizedDescription)
                return
            }
            
            var users: [UserMoodel] = []
            for json in result.arrayValue {
                let user = UserMoodel(json: json)
                users.append(user)
            }
            self.users = users
            self.tbView.reloadData()
        }
    }
}

extension UserToolsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbView.dequeue(UserTableViewCell.self)
        let user = users[indexPath.row]
        cell.bindUser(user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tbView.deselectRow(at: indexPath, animated: true)
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let user = users[indexPath.row]
        
        actionSheet.addAction(UIAlertAction(title: "Enroll user",
                                            style: .default, handler: { [unowned self] _ in
                                                self.enrollAction(user)
        }))
        actionSheet.addAction(UIAlertAction(title: "Change user status",
                                            style: .default, handler: { [unowned self] _ in
                                                self.changeStatusAction(user)
        }))
        actionSheet.addAction(UIAlertAction(title: "Delete",
                                            style: .default, handler: { [unowned self] _ in
                                                self.delete(user)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
}

extension UserToolsViewController {
    
    private func createUser(name: String, email: String) {
        var params: [String: String] = [:]
        params["username"] = name
        params["email"] = email
        startAnimating()
        APIService.instance.sendRequest(API.createUser, params: params) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard result != nil else {
                self.showAlert(error.localizedDescription)
                return
            }
            self.loadUser()
        }
    }
    
    private func enrollAction(_ user: UserMoodel) {
        if user.email.isEmpty || user.username.isEmpty {
            self.showAlert("Can't enroll this user")
            return
        }

        var params: [String: String] = [:]
        params["username"] = user.username
        params["email"] = user.email
        
        startAnimating()
        APIService.instance.sendRequest(API.enrollUser, params: params) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard let result = result else {
                self.showAlert(error.localizedDescription)
                return
            }
  
            self.showAlert("Enroll Success \(result.description)")
        }
    }
    
    private func changeStatusAction(_ user: UserMoodel) {
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "active",
                                            style: .default, handler: { [unowned self] _ in
                                                self.modifyUser(user, status: "active")
        }))
        actionSheet.addAction(UIAlertAction(title: "disabled",
                                            style: .default, handler: { [unowned self] _ in
                                                self.modifyUser(user, status: "disabled")
        }))
        actionSheet.addAction(UIAlertAction(title: "bypass",
                                            style: .default, handler: { [unowned self] _ in
                                                self.modifyUser(user, status: "bypass")
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func modifyUser(_ user: UserMoodel, status: String) {
        guard user.status != status else {
            self.showAlert("status already exists")
            return
        }
        
        var params: [String: String] = [:]
        params["status"] = status
        
        startAnimating()
        APIService.instance
            .sendRequest(API.modifyUser(id: user.userId), params: params) { [weak self] (result, error) in
                guard let `self` = self else { return }
                self.stopAnimating()
                guard result != nil else {
                    self.showAlert(error.localizedDescription)
                    return
                }
                user.status = status
                self.tbView.reloadData()
                self.showAlert("Status has changed")
        }
    }
    
    private func delete(_ user: UserMoodel) {
        startAnimating()
        APIService.instance
            .sendRequest(API.deleteUser(id: user.userId)) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard result != nil else {
                self.showAlert(error.localizedDescription)
                return
            }
            self.loadUser()
        }
    }
}
