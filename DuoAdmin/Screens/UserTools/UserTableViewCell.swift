//
//  UserTableViewCell.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var statusLabel: UILabel!
    @IBOutlet weak private var emailLabel: UILabel!
    @IBOutlet weak private var phoneLabel: UILabel!
    
    func bindUser(_ user: UserMoodel) {
        nameLabel.text = user.username
        statusLabel.text = user.status
        emailLabel.text = user.email
        phoneLabel.text = user.phones.first?.number
    }
    
    func bindAdmin(_ admin: AdminModel) {
        nameLabel.text = admin.name
        statusLabel.text = admin.role
        emailLabel.text = admin.email
        phoneLabel.text = admin.phone
    }
}
