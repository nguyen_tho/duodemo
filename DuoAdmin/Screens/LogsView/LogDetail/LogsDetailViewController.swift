//
//  LogsDetailViewController.swift
//  DuoAdmin
//
//  Created by Nguyen Manh Tuan on 1/21/19.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class LogsDetailViewController: BaseViewController {

    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var logsTextView: UITextView!
    
    private var logs: String = ""
    private var titleStr: String = ""
    
    convenience init(logs: String, title: String) {
        self.init()
        self.logs = logs
        self.titleStr = title
    }
    
    override func setupView() {
        logsTextView.text = logs
        self.titleLabel.text = titleStr
    }

}
