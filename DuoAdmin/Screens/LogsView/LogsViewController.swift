//
//  LogsViewController.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class LogsViewController: BaseViewController {

    private var logTitle: String = ""
    
    @IBAction func authLogs(_ sender: Any) {
        logTitle = "Authentication Logs"
        var params: [String : String] = [:]
        
        let maxTime = Int(Date().timeIntervalSince1970 * 1000)
        let minTime = maxTime - 7 * 24 * 60 * 60 * 1000
            
        params["mintime"] = "\(minTime)"
        params["maxtime"] = "\(maxTime)"
        callAPIgetLog(api: API.authLogs, params: params)
    }
    
    @IBAction func telephonyLogs(_ sender: Any) {
        logTitle = "Administrator Logs"
        callAPIgetLog(api: API.telephonyLogs, params: [:])
    }
    
    @IBAction func adminLogs(_ sender: Any) {
        logTitle = "Telephony Logs"
        callAPIgetLog(api: API.adminLogs, params: [:])
    }
    
    private func callAPIgetLog(api: API, params: [String: String]) {
        startAnimating()
        APIService.instance.sendRequest(api, params: params) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard let result = result else {
                self.showAlert(error.localizedDescription)
                return
            }
            let logs = result.description
            self.showLogDetail(logs)
        }
    }
    
    private func showLogDetail(_ log: String) {
        let logDetail = LogsDetailViewController(logs: log, title: logTitle)
        self.navigationController?.pushViewController(logDetail, animated: true)
    }
}
