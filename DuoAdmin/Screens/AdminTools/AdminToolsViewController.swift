//
//  AdminToolsViewController.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class AdminToolsViewController: BaseViewController {
    
    private var users: [AdminModel] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadAdmin()
    }
    
    override func setupView() {
        tableView.tableFooterView = UIView()
        tableView.register(UserTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func createAdmin(_ sender: Any) {
        let createAdmin = CreateAdminViewController()
        self.navigationController?.pushViewController(createAdmin, animated: true)
    }
    
    private func loadAdmin() {
        startAnimating()
        APIService.instance.sendRequest(API.getAdmins) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard let result = result else {
                self.showAlert(error.localizedDescription)
                return
            }
            
            var admins: [AdminModel] = []
            for json in result.arrayValue {
                let user = AdminModel(json: json)
                admins.append(user)
            }
            self.users = admins
            self.tableView.reloadData()
        }
    }
}

extension AdminToolsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(UserTableViewCell.self)
        let user = users[indexPath.row]
        cell.bindAdmin(user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let user = users[indexPath.row]
        
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Delete",
                                            style: .default, handler: { [unowned self] _ in
                                                self.deleteUser(user)
        }))
        actionSheet.addAction(UIAlertAction(title: "Change Phone",
                                            style: .default, handler: { [unowned self] _ in
                                                self.changePhone(user)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
}

extension AdminToolsViewController {
    
    private func deleteUser(_ user: AdminModel) {
        startAnimating()
        APIService.instance.sendRequest(API.deleteAdmin(id: user.adminId)) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard result != nil else {
                self.showAlert(error.localizedDescription)
                return
            }
            self.loadAdmin()
        }
    }
    
    private func changePhone(_ user: AdminModel) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "New Phone number"
        }
        alertController.addAction(
            UIAlertAction(title: "OK", style: .default, handler: { [unowned self] _ in
                guard let textField = alertController.textFields?.first else { return }
                guard let phone = textField.text, !phone.isEmpty else { return }
                self.updatePhone(phone, user: user)
            }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    private func updatePhone(_ phone: String, user: AdminModel) {
        let params: [String: String] = ["phone" : phone]
        startAnimating()
        APIService.instance
            .sendRequest(API.modifyAdmin(id: user.adminId), params: params) { [weak self] (result, error) in
                guard let `self` = self else { return }
                self.stopAnimating()
                guard result != nil else {
                    self.showAlert(error.localizedDescription)
                    return
                }
                user.phone = phone
                self.tableView.reloadData()
        }
    }
    
}
