//
//  CreateAdminViewController.swift
//  DuoAdmin
//
//  Created by Nguyen Manh Tuan on 1/21/19.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class CreateAdminViewController: BaseViewController {

    @IBOutlet weak private var emailTf: UITextField!
    @IBOutlet weak private var passTf: UITextField!
    @IBOutlet weak private var nameTf: UITextField!
    @IBOutlet weak private var phoneTf: UITextField!
    
    @IBAction func createAction(_ sender: Any) {
        if !createUser() {
            self.showAlert("Missing or malformed information!")
        }
    }
    
    private func createUser() -> Bool {
        guard let email = emailTf.text, !email.isEmpty, email.isValidEmail() else { return false }
        guard let pass = passTf.text, !pass.isEmpty else { return false }
        guard let name = nameTf.text, !name.isEmpty else { return false }
        guard let phone = phoneTf.text, !phone.isEmpty else { return false }
        
        var params: [String: String] = [:]
        params["email"] = email
        params["password"] = pass
        params["name"] = name
        params["phone"] = phone
        
        startAnimating()
        APIService.instance
            .sendRequest(API.createAdmin, params: params) { [weak self] (result, error) in
                guard let `self` = self else { return }
                self.stopAnimating()
                guard result != nil else {
                    self.showAlert(error.localizedDescription)
                    return
                }
                self.showAlert("Create Admin Success", completeHandle: { [unowned self] in
                    self.navigationController?.popViewController(animated: true)
                })
        }
        return true
    }
    
    
}
