//
//  SettingViewController.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {
    
    @IBOutlet private weak var iaKey: UITextField!
    @IBOutlet private weak var secretKey: UITextField!
    @IBOutlet private weak var host: UITextField!
    @IBOutlet private weak var submitButton: UIButton!
    
    override func setupView() {
        iaKey.text = Constants.DuoKey.ikey
        secretKey.text = Constants.DuoKey.skey
        host.text = Constants.DuoKey.host
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        guard let ikey = iaKey.text, !ikey.isEmpty else { return }
        guard let skey = secretKey.text, !skey.isEmpty else { return }
        guard let host = host.text, !host.isEmpty else { return }
        
        UserDefaultHelper.saveValue(ikey, key: Constants.DuoKeyWord.ikey)
        UserDefaultHelper.saveValue(skey, key: Constants.DuoKeyWord.skey)
        UserDefaultHelper.saveValue(host, key: Constants.DuoKeyWord.host)
        
        Constants.DuoKey.ikey = ikey
        Constants.DuoKey.skey = skey
        Constants.DuoKey.host = host
    }
}
