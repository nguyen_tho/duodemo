//
//  HomeViewController.swift
//  DuoMobile
//
//  Created by Nguyen Manh Tuan on 1/20/19.
//  Copyright © 2019 Nguyen Manh Tuan. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet private weak var creditReportLabel: UILabel!
    
    @IBAction func userAction(_ sender: Any) {
        let user = UserToolsViewController()
        self.navigationController?.pushViewController(user, animated: true)
    }
    
    @IBAction func adminAction(_ sender: Any) {
        let admin = AdminToolsViewController()
        self.navigationController?.pushViewController(admin, animated: true)
    }
    
    @IBAction func logAction(_ sender: Any) {
        let logs = LogsViewController()
        self.navigationController?.pushViewController(logs, animated: true)
    }
    
    @IBAction func settingAction(_ sender: Any) {
        let setting = SettingViewController()
        self.navigationController?.pushViewController(setting, animated: true)
    }
    
    @IBAction func refreshAction(_ sender: Any) {
        startAnimating()
        APIService.instance.sendRequest(API.creditReport) { [weak self] (result, error) in
            guard let `self` = self else { return }
            self.stopAnimating()
            guard let result = result else {
                self.showAlert(error.localizedDescription)
                return
            }
            let creditUsed = result["telephony_credits_used"].floatValue
            self.creditReportLabel.text = "\(creditUsed)"
        }
    }
    
}
