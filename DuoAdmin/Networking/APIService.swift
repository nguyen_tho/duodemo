//
//  APIService.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import Foundation

import Foundation
import Alamofire
import RxSwift
import SwiftyJSON

enum Result<T> {
    case success(T)
    case error(NSError)
}

final class APIService {
    
    private(set) var sessionManager: SessionManager!
    private let disposeBag = DisposeBag()
    
    static let instance: APIService = {
        let service = APIService()
        service.setHeader()
        return service
    }()
    
    private func setHeader() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
}
extension APIService {
    
    func baseRequestAPI(_ api: API,
                        params: [String: String] = [:]) -> Observable<Result<JSON>> {
        
        let url = "https://\(Constants.DuoKey.host)\(api.path)"
        
        guard let path = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            return Observable.just(Result.error(NSError.make(message: "url error", code: 0)))
        }
        
        return Observable.create { observer -> Disposable in
            let request = APIService.instance
                .sessionManager
                .request(path,
                         method: api.method,
                         parameters: params,
                         encoding: JSONEncoding.default,
                         headers: nil)
            request
                .authenticate(user: "", password: "")
                .responseJSON { response in
                    let statusCode = response.response?.statusCode ?? 0
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        Log.debug(message: "\(path) RESPONSE SUCCESS \(json)")
                        let status = json["status"].int ?? 1
                        if 200 ... 300 ~= statusCode && status != 0 {
                            observer.onNext(Result.success(json))
                        } else {
                            let message = json["message"].stringValue
                            let error = NSError.make(message: message, code: status)
                            observer.onNext(Result.error(error))
                        }
                    case .failure(let error):
                        Log.debug(message: "\(path) RESPONSE ERROR \(error.localizedDescription)")
                        if  (error as NSError).code != NSURLErrorCancelled {
                            observer.onNext(Result.error(error as NSError))
                        }
                    }
            }
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func sendRequest(_ api: API,
                     params: [String: String] = [:],
                     completionHandler: @escaping (_ result: JSON?, _ error: NSError) -> Void) {
        
        var uri: String = ""
        var body: String = ""
        let now = Util.rfc2822Date(Date())
        let normalizedParams = Util.normalizeParams(params)
        let canonicalizedParams: String = Util.canonicalizeParams(normalizedParams)
        
        let authHeader = Util.basicAuth(Constants.DuoKey.ikey,
                                        skey: Constants.DuoKey.skey,
                                        method: api.method.rawValue.uppercased(),
                                        host: Constants.DuoKey.host,
                                        path: api.path,
                                        dateString: now,
                                        params: normalizedParams)
        
        var headder: HTTPHeaders = [:]
        headder[Constants.Server.auth] = authHeader
        headder[Constants.Server.date] = now
        headder[Constants.Server.host] = Constants.DuoKey.host
        headder[Constants.Server.agent] = "Duo API Swift \(Constants.Server.version)"
        if api.method == .post || api.method == .put {
            headder[Constants.Server.contentType] = "application/x-www-form-urlencoded"
            uri = api.path
            body = canonicalizedParams
        } else {
            uri = canonicalizedParams != "" ? "\(api.path)?\(canonicalizedParams)" : api.path
        }

        let url = "https://\(Constants.DuoKey.host)\(uri)"
        let getURL = URL(string: url)!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = api.method.rawValue.uppercased()
        getRequest.allHTTPHeaderFields = headder
        getRequest.httpBody = body.data(using: .utf8)
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            guard let data = data else {
                let requestError = error?.localizedDescription ?? "Empty data"
                print("GET Request: Communication error: \(requestError)")
                completionHandler(nil, NSError.make(message: requestError))
                return
            }
            
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: [])
                let json = JSON(resultObject)
                guard json["response"] != JSON.null else {
                    let msgError = json["message"].stringValue
                    DispatchQueue.main.async {
                        completionHandler(nil, NSError.make(message: msgError))
                        print("Results from GET \(url):\n\(resultObject)")
                    }
                    return
                }
                DispatchQueue.main.async(execute: {
                    completionHandler(json["response"], NSError.make(message: ""))
                    print("Results from GET \(url):\n\(resultObject)") })
            } catch {
                DispatchQueue.main.async(execute: {
                    completionHandler(nil, NSError.make(message: "Unable to parse JSON response"))
                    print("Unable to parse JSON response")
                })
            }
            
        }).resume()
    }
    
}
