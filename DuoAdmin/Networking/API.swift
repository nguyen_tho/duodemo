//
//  API.swift
//  eschool
//
//  Created by nguyen.manh.tuanb on 8/15/18.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import Foundation
import Alamofire

enum API {
    case users
    case token
    case ping
    case check
    
    case creditReport
    case createUser
    case enrollUser
    case modifyUser(id: String)
    case deleteUser(id: String)
    
    case getAdmins
    case createAdmin
    case deleteAdmin(id: String)
    case modifyAdmin(id: String)
    
    case authLogs
    case adminLogs
    case telephonyLogs
}

extension API {
    var baseURL: URL {
        return URL(string: Constants.Server.domainName)!
    }

    var path: String {
        switch self {
        case .users:
            return "/admin/v1/users"
        case .token:
            return "/admin/v1/tokens"
        case .ping:
            return "/auth/v2/ping"
        case .check:
            return "/auth/v2/check"
        case .creditReport:
            return "/admin/v1/info/telephony_credits_used"
        case .enrollUser:
            return "/admin/v1/users/enroll"
        case .modifyUser(let id):
            return "/admin/v1/users/\(id)"
        case .createAdmin:
            return "/admin/v1/admins"
        case .getAdmins:
            return "/admin/v1/admins"
        case .deleteAdmin(let id):
            return "/admin/v1/admins/\(id)"
        case .modifyAdmin(let id):
            return "/admin/v1/admins/\(id)"
        case .authLogs:
            return "/admin/v2/logs/authentication"
        case .adminLogs:
            return "/admin/v1/logs/administrator"
        case .telephonyLogs:
            return "/admin/v1/logs/telephony"
        case .createUser:
            return "/admin/v1/users"
        case .deleteUser(let id):
            return "/admin/v1/users/\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .modifyUser, .enrollUser, .createAdmin, .modifyAdmin, .createUser:
            return .post
        case .deleteAdmin, .deleteUser:
            return .delete
        default:
            return .get
        }
    }
}
