//
//  AdminModel.swift
//  DuoAdmin
//
//  Created by Nguyen Manh Tuan on 1/21/19.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import Foundation
import SwiftyJSON

final class AdminModel {
    var adminId: String
    var email: String
    var name: String
    var phone: String
    var role: String
    var restricted_by_admin_units: Bool
    
    init(json: JSON) {
        adminId = json["admin_id"].stringValue
        email = json["email"].stringValue
        name = json["name"].stringValue
        phone = json["phone"].stringValue
        role = json["role"].stringValue
        restricted_by_admin_units = json["restricted_by_admin_units"].boolValue
    }
}
