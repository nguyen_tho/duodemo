//
//  UserModel.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import Foundation
import SwiftyJSON

final class UserMoodel {
    var userId: String
    var username: String
    var phones: [Phone] = []
    var email: String
    var realname: String
    var status: String
    
    init(json: JSON) {
        userId = json["user_id"].stringValue
        username = json["username"].stringValue
        for phoneJson in json["phones"].arrayValue {
            let phone = Phone(json: phoneJson)
            phones.append(phone)
        }
        email = json["email"].stringValue
        realname = json["realname"].stringValue
        status = json["status"].stringValue
    }
}

struct Phone {
    var number: String
    var activated: Bool
    var phoneId: String
    
    init(json: JSON) {
        number = json["number"].stringValue
        activated = json["activated"].boolValue
        phoneId = json["phone_id"].stringValue
    }
}
