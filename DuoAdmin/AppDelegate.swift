//
//  AppDelegate.swift
//  DuoAdmin
//
//  Created by nguyen.manh.tuanb on 21/01/2019.
//  Copyright © 2019 nguyen.manh.tuanb. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        loadDuoKey()
        
        let home = HomeViewController()
        let navigation = UINavigationController(rootViewController: home)
        setRootViewController(navigation)

        return true
    }
    
    private func loadDuoKey() {
        guard let ikey = UserDefaultHelper.getValue(key: Constants.DuoKeyWord.ikey) else { return }
        guard let skey = UserDefaultHelper.getValue(key: Constants.DuoKeyWord.skey) else { return }
        guard let host = UserDefaultHelper.getValue(key: Constants.DuoKeyWord.host) else { return }
        
        Constants.DuoKey.ikey = ikey
        Constants.DuoKey.skey = skey
        Constants.DuoKey.host = host
    }
}

