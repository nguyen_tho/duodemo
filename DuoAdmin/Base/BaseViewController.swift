//
//  BaseViewController.swift
//  ThachSanh
//
//  Created by nguyen.manh.tuanb on 10/10/2018.
//  Copyright © 2018 nguyen.manh.tuanb. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import NVActivityIndicatorView

class BaseViewController: UIViewController {
    
    internal let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupView()
        setupRx()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    internal func setupViewModel() { }
    internal func setupView() { }
    internal func setupRx() { }
    
    internal func showAlert(_ message: String, completeHandle: (() -> Void)? = nil) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            completeHandle?()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    internal func startAnimating() {
        
        let data = ActivityData(size: CGSize(width: 100, height: 40),
                                type: NVActivityIndicatorType.ballPulseSync,
                                padding: 0)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(data, nil)
    }
    
    internal func stopAnimating() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
