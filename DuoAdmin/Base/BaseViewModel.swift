//
//  BaseViewModel.swift
//  GogoCar
//
//  Created by nguyen.manh.tuanb on 8/28/18.
//  Copyright © 2018 Nguyen Manh Tuan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseViewModel {
    internal var disposeBag = DisposeBag()
    internal let activityIndicator = ActivityIndicator()
}
